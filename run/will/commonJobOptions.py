if 'LayoutOption' not in locals() : 
  LayoutOption = 'InclinedAlternative' #default layout option
  #other options are: InclinedQuads, InclinedDuals, ... 

from AthenaCommon.DetFlags import DetFlags
from AthenaCommon.GlobalFlags import globalflags


globalflags.DataSource='geant4'
globalflags.DetDescrVersion = 'ATLAS-P2-ITK-20-00-00'   #forces InDetDescrCnv to look for dictionary at InDetIdDictFiles/IdDictInnerDetector_SLHC_InclBrl_4.xml

#
#==============================================================
# Load Detector Description for Inner Detector.
#==============================================================
#

DetFlags.all_setOff()
DetFlags.detdescr.bpipe_setOn() #necessary??
DetFlags.detdescr.pixel_setOn() #for pixel ITk
#DetFlags.detdescr.SCT_setOn() #for strip ITk


# import the the conditions setup
from IOVDbSvc.CondDB import conddb
conddb.setGlobalTag('OFLCOND-MC15c-SDR-13')
       
# Initialize geometry
log.info("Initializing Geometry")
from AtlasGeoModel import GeoModelInit
from AtlasGeoModel import SetGeometryVersion

# --- setup version ... next two lines are equivalent of preInclude.SLHC.py
from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags
SLHC_Flags.SLHC_Version = ''

##****** CHANGE THE NEXT LINE TO CHANGE LAYOUTS *******
SLHC_Flags.LayoutOption = LayoutOption


if SLHC_Flags.LayoutOption() == "InclinedDuals":
  pixLayout = "InclBrl4Ref_InclinedDuals"
  pixendcapLayout = "ECRing4Ref_InclinedDuals"
  PIXELGENERAL = "InclBrl4_PixelGeneral_InclinedDuals"
  STAVESUPPORT = "InclBrl4_SlimStaveSupport_InclinedDuals"
  PIXELDISCSUPPORT = "InclBrl4_InclinedDuals_DiskSupport"
  PIXELROUTINGSERVICE = "InclBrl4_InclinedDuals_PixelRoutingService"
elif SLHC_Flags.LayoutOption()=="InclinedQuads":
  pixLayout = "InclBrl4Ref_InclinedQuads"
  pixendcapLayout = "ECRing4Ref_InclinedQuads"
  #these files are in BarrelInclinedRef
  PIXELGENERAL = "InclBrl4_PixelGeneral_InclinedQuads"
  STAVESUPPORT = "InclBrl4_SlimStaveSupport_InclinedQuads"
  PIXELDISCSUPPORT = "InclBrl4_InclinedQuads_DiskSupport"
  PIXELROUTINGSERVICE = "InclBrl4_InclinedQuads_PixelRoutingService"
elif SLHC_Flags.LayoutOption()=="InclinedAlternative":
  #these files in InDetTrackingGeometryXML
  pixLayout = "InclBrl4Ref_InclinedAlternative"
  pixendcapLayout = "ECRing4Ref_InclinedAlternative"
  #these files are in BarrelInclinedRef
  PIXELGENERAL = "InclBrl4_PixelGeneral_InclinedAlternative"
  STAVESUPPORT = "InclBrl4_SlimStaveSupport_InclinedAlternative"
  PIXELDISCSUPPORT = "InclBrl4_InclinedAlternative_DiskSupport"
  PIXELROUTINGSERVICE = "InclBrl4_InclinedAlterntive_PixelRoutingService"


#finds/reads files in InDetTrackingGeometryXML

#positions of the sensors (active material only)
#the 'surfaces'
log.info("Configuring for layout from XML")
from InDetSLHC_Example.SLHC_Setup_XML import SLHC_Setup_XMLReader
SLHC_Setup_XMLReader( PixelLayout = pixLayout,
                      PixelEndcapLayout = pixendcapLayout,
                      SCTLayout = "FourLayersNoStub_23-25-dev0",
                      dictionaryFileName = "InDetIdDictFiles/IdDictInnerDetector_SLHC_InclBrl_4.xml",
                      createXML = True,
                      doPix=DetFlags.detdescr.pixel_on(),
                      doSCT=DetFlags.detdescr.SCT_on(), ##WB was false
                      isGMX=True , ##WB was false #what is GMX????
                      addBCL=False, #True # If you want to set this to True, you must also change InnerDetector/InDetDetDescr/GmxLayouts/cmt/requirements to point to the Strips layout containing the BCL, otherwise Reco will segfault - Ben
                      )




# GeoModelConfiguration 

xmlFileDict={}
#common
#positions of everything else: material, supports, services ... and the TYPE of sensor
xmlFileDict["Pixel"]={
      "PIXELSIMPLESERVICE":"InclBrl_PixelSimpleService",
      "MATERIAL":"InclBrl_Material",
      "SILICONMODULES":"ITK_PixelModules",
      "SILICONREADOUT":"PixelModuleReadout"
}

if "PIXELGENERAL" in locals(): xmlFileDict["Pixel"].update({"PIXELGENERAL":PIXELGENERAL})
if "STAVESUPPORT" in locals(): xmlFileDict["Pixel"].update({"STAVESUPPORT":STAVESUPPORT})
if "PIXELDISCSUPPORT" in locals(): xmlFileDict["Pixel"].update({"PIXELDISCSUPPORT":PIXELDISCSUPPORT})
if "PIXELROUTINGSERVICE" in locals(): xmlFileDict["Pixel"].update({"PIXELROUTINGSERVICE":PIXELROUTINGSERVICE})

log.info("Setting environment variables")

for key in xmlFileDict["Pixel"].keys():
   #apparently we need to set environment variables ... apparently needed by PixelDesignSvc (an instance of PixelDesignBuilder) [PixelModuleTools pkg]
   fileName=xmlFileDict["Pixel"][key]+".xml"
   envName="PIXEL_"+key+"_GEO_XML"
   os.environ[envName]=fileName
   print "ENV ",envName," ",fileName

svcMgr+=CfgMgr.PixelModuleBuilder("PixelModuleSvc")
svcMgr+=CfgMgr.PixelDesignBuilder("PixelDesignSvc")

ToolSvc+=CfgMgr.PixelServicesTool("PixelServicesTool",ReadSvcFromDB=True,SvcDynAutomated=False,BarrelModuleMaterial=True)




ToolSvc+=CfgMgr.GeoPixelLayerInclRefTool("InnerPixelLayerTool")                                              #from BarrelInclinedRef pkg
ToolSvc+=CfgMgr.GeoPixelLayerPlanarRefTool(name="PlanarPixelLayerTool")
#ToolSvc+=CfgMgr.PixelLayerValidationTool("LayerValidationTool",PixelServicesTool=ToolSvc.PixelServicesTool)
#ToolSvc.InnerPixelLayerTool.LayerValidationTool=ToolSvc.LayerValidationTool
ToolSvc+=CfgMgr.GeoPixelBarrelInclRefTool("GeoPixelBarrelInclRefTool",                                       #from BarrelInclinedRef pkg
                                          InnerPixelLayerTool = ToolSvc.InnerPixelLayerTool,
                                          MaxInnerLayerMax = 5,
                                          PixelServicesTool = ToolSvc.PixelServicesTool)

if SLHC_Flags.LayoutOption() == "InclinedAlternative":
  ToolSvc.GeoPixelBarrelInclRefTool.InnerPixelLayerTool = ToolSvc.PlanarPixelLayerTool #needed when using IBL stave type instead of Alpine type
  ToolSvc.GeoPixelBarrelInclRefTool.OuterPixelLayerTool = ToolSvc.InnerPixelLayerTool
  ToolSvc.GeoPixelBarrelInclRefTool.MaxInnerLayerMax = 2


ToolSvc+=CfgMgr.GeoPixelLayerECRingRefTool("GeoPixelLayerECRingRefTool")                                      #from EndcapRingRef pkg
ToolSvc+=CfgMgr.GeoPixelEndcapECRingRefTool("GeoPixelEndcapECRingRefTool",                                    #from EndcapRingRef pkg
                                            GeoPixelEndcapLayerTool=ToolSvc.GeoPixelLayerECRingRefTool,
                                            PixelServicesTool=ToolSvc.PixelServicesTool)

ToolSvc+=CfgMgr.GeoPixelEnvelopeInclRefTool("GeoPixelEnvelopeInclRefTool",                                    #from BarrelInclinedRef pkg
                                            GeoPixelBarrelTool=ToolSvc.GeoPixelBarrelInclRefTool,
                                            GeoPixelEndcapTool=ToolSvc.GeoPixelEndcapECRingRefTool,
                                            PixelServicesTool =ToolSvc.PixelServicesTool)


pixelTool = svcMgr.GeoModelSvc.DetectorTools['PixelDetectorTool']
pixelTool.Alignable = False
pixelTool.FastBuildGeoModel = True
pixelTool.ConfigGeoAlgTool = True
pixelTool.ReadXMLFromDB = False
pixelTool.ConfigGeoBase = "GeoPixelEnvelopeInclRefTool"






topSequence = CfgMgr.AthSequencer("AthAlgSeq")
svcMgr.MessageSvc.infoLimit        = 100000
svcMgr.MessageSvc.debugLimit        = 100000









