include(os.environ['TestArea']+"/run/will/commomJobOptions.py")

#--------------------------------------------------------------
# To display the tracking geometry
#--------------------------------------------------------------
# --- setup of tracking geometry

from TrkDetDescrSvc.TrkDetDescrJobProperties import TrkDetFlags
TrkDetFlags.SLHC_Geometry                   = True
#TrkDetFlags.PixelBuildingOutputLevel        = VERBOSE
#TrkDetFlags.SCT_BuildingOutputLevel         = VERBOSE
#TrkDetFlags.TRT_BuildingOutputLevel         = VERBOSE
TrkDetFlags.MagneticFieldCallbackEnforced   = False
TrkDetFlags.TRT_BuildStrawLayers            = False
TrkDetFlags.MaterialDatabaseLocal           =  SLHC_Flags.SLHC_Version() != '' 
TrkDetFlags.MaterialStoreGateKey            = '/GLOBAL/TrackingGeo/LayerMaterialITK'
#TrkDetFlags.MaterialTagBase                 = 'AtlasLayerMat_v' ???
TrkDetFlags.MaterialVersion                 = 20
TrkDetFlags.MaterialSubVersion              = ""
TrkDetFlags.MaterialMagicTag                = globalflags.DetDescrVersion()


###------------------------
###Pulled from InDetSLHC_Example/SLHC_Setup_ITk_TrackingGeometry.py
TrkDetFlags.XMLFastCustomGeometry = True
TrkDetFlags.InDetTrackingGeometryBuilderName = 'InDetTrackingGeometryBuilder'
# init FastGeoModel geometry builder
from InDetTrackingGeometryXML.ConfiguredSLHC_InDetTrackingGeometryXMLBuilder import ConfiguredSLHC_InDetTrackingGeometryXMLBuilder
ToolSvc += ConfiguredSLHC_InDetTrackingGeometryXMLBuilder(name='InDetTrackingGeometryBuilder')
# load the tracking geometry service
from TrkDetDescrSvc.AtlasTrackingGeometrySvc import AtlasTrackingGeometrySvc
###------------------------




#tracking configuration


# the magnetic field
from MagFieldServices import SetupField
conddb.addOverride('/GLOBAL/BField/Map','BFieldMap-FullAsym-09-solTil3')

from TrkExEngine.AtlasExtrapolationEngine import AtlasExtrapolationEngine
ToolSvc += AtlasExtrapolationEngine(name='Extrapolation', nameprefix='Atlas', ToolOutputLevel=INFO)
ToolSvc += CfgMgr.Trk__PositionMomentumWriter("PmWriter")

ExtrapolationEngineTest = CfgMgr.Trk__ExtrapolationEngineTest('ExtrapolationEngineTest')
# how many tests you want per event 
ExtrapolationEngineTest.NumberOfTestsPerEvent   = 100
# parameters mode: 0 - neutral tracks, 1 - charged particles 
ExtrapolationEngineTest.ParametersMode          = 1
# do the full test backwards as well            
ExtrapolationEngineTest.BackExtrapolation       = False
# Smear the production vertex - standard primary vertex paramters
ExtrapolationEngineTest.SmearOrigin             = False
ExtrapolationEngineTest.SimgaOriginD0           = 2./3.
ExtrapolationEngineTest.SimgaOriginZ0           = 50.
ExtrapolationEngineTest.SmearFlatOriginZ0       = True
ExtrapolationEngineTest.Z0Min                   =  150.
ExtrapolationEngineTest.Z0Max                   =  150.
#ExtrapolationEngineTest.Z0Values                = [-150., 0., 150.]
ExtrapolationEngineTest.SmearFlatOriginD0       = True
ExtrapolationEngineTest.D0Min                   = .0
ExtrapolationEngineTest.D0Max                   = .0
# pT range for testing                        
ExtrapolationEngineTest.PtMin                   = 1000
ExtrapolationEngineTest.PtMax                   = 1000
# The test range in Eta                      
ExtrapolationEngineTest.EtaMin                  =  -4.
ExtrapolationEngineTest.EtaMax                  =   4.
#ExtrapolationEngineTest.PhiMin                  =  -1.81
#ExtrapolationEngineTest.PhiMax                  =  -1.79
# Configure how you wanna run                  
ExtrapolationEngineTest.CollectSensitive        = True
ExtrapolationEngineTest.CollectPassive          = True
ExtrapolationEngineTest.CollectBoundary         = True
ExtrapolationEngineTest.CollectMaterial         = True
# the path limit to test                        
ExtrapolationEngineTest.PathLimit               = -1.
# give it the engine
ExtrapolationEngineTest.ExtrapolationEngine     = ToolSvc.AtlasExtrapolation
# validation tool 
ExtrapolationEngineTest.PositionMomentumWriter  = ToolSvc.PmWriter

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += [ "val DATAFILE='ExtrapolationEngineTest_InclBrl4_quads_1GeV_Steps.root' TYPE='ROOT' OPT='RECREATE'" ]



#add extrapolation output algorithm
theApp.EvtMax = 2000
topSequence += ExtrapolationEngineTest
