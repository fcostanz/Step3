2018-02-22 Francesco Costanza <francesco.costanza@cern.ch>
	* fix of clashes
	* Type0/Type1 cables
	* tagget at BarrelInclinedRef-00-00-85

2017-12-12 Sharka Todorova <sarka.todorova@cern.ch>
	* merge with -00-00-76-07 branch tag
	* outer routing for barrel L3 in LightBrlv2
	* tagged at BarrelInclinedRef-00-00-84

2017-11-08 Sharka Todorova <sarka.todorova@cern.ch>
	* bugfix for the local support material
	* tagged at BarrelInclinedRef-00-00-83

2017-10-25 Francesco Costanza <francesco.costanza@cern.ch>
	* Service fix for InclinedDuals
	* tagged at BarrelInclinedRef-00-00-82

2017-10-20 Sharka Todorova <sarka.todorova@cern.ch>
	* material update
	* tagged at BarrelInclinedRef-00-00-81

2017-10-19 Sharka Todorova <sarka.todorova@cern.ch>
	* merge with -00-00-76-03 branch tag
	* tagged at BarrelInclinedRef-00-00-80

2017-10-19 Sharka Todorova <sarka.todorova@cern.ch>
	* longeron-like material for L2-L4 barrel layers of LightBrlv2
	* configurable build-up of pixel tracking volume
	* tagged at BarrelInclinedRef-00-00-79

2017-07-17 Sharka Todorova <sarka.todorova@cern.ch>
	* services for alternating staves 
	* tagged at BarrelInclinedRef-00-00-78

2017-07-13 Sharka Todorova <sarka.todorova@cern.ch>
	* width of local support configurable 
	* import light barrel setup
	* tagged at BarrelInclinedRef-00-00-77

2017-07-12 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated InclinedDual supports to use a fixed weight

2017-07-05 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Fixed IExtBrl4 crash due to malformed XML

2017-07-05 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Reverted MountainEdge for InclinedQuads/Duals from 5mm to 0.01mm (collision error)
	* Updated volume calculation for standard stave
	* Tagged  as BarrelInclinedRef-00-00-75	

2017-07-04 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated Longeron mass calculation, correct mass now used

2017-07-03 Ben Smart <ben.smart@cern.ch>
	* Updating serivces to match 4MHz trigger readout rate estimate

2017-07-03 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated longeron corner dimensions
2017-06-30 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated longeron description to use corners
	* Added new materials for longeron corners (82% mass in corners)
	* Updated definitions longeron definitions for incliend quads and duals
2017-06-29 Sharka Todorova <sarka.todorova@cern.ch>
	* set the edge of dual foam support to 5 mm

2017-06-28 Sharka Todorova <sarka.todorova@cern.ch>
	* add radial tilt for inclined modules ( needed for alternating barrel L0 staves )
	* cleanup the hardcoded svc offset 

2017-06-18 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated service material estimates for InclinedDuals and InclinedQuads

2017-06-18 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated longeron material masses for Inclined Duals
	* Turned on longeron corners (4mm) for Inclined Quads and Duals

2017-06-14 Ben Smart <ben.smart@cern.ch>
	* Adding new chip type materials for 3D sensors to include 100 microns of Silicon support wafer

2017-06-13 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Fixed XML for InclBrl4_OptRing

2017-06-12 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Corrected Service module names
2017-06-09 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* updated longeron shapes for Inclined Duals
	* Known_Issues: L3 service placement in error (collision)
	* Known_Issues: longeron materials need updating
	* Fixed L3 Service placement error
2017-06-07 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Fixed incorrectly placed L3-L4 longeron
	* Fixed bugs associated with phi offset of layers
	* Added InclinedDual XML files
	* Updated longeron z dimenasiosn for InclinedDual

2017-06-07 Ben Smart <ben.smart@cern.ch>
	* Fixing end-cap support tube radii

2017-06-07 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Hotfix for missing longeron 
	* Tagging as InDetTrackingGeometryXML-00-00-67
2017-06-06 Noemi Calace <noemi.calace@cern.ch>
	* Tagging all the latest developments
	* InDetTrackingGeometryXML-00-00-66

2017-06-06 Noemi Calace <noemi.calace@cern.ch>
	* Adding material for entire ring back. 
	* Better to have it for compatibility with previous layouts
2017-06-04 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated inclined quads longeron XML to 2017_04(April)_20_Step2.0_layout
2017-05-31 Ben Smart <ben.smart@cern.ch>
	* Adding new material for innermost rings. It is a scaled version of the UK rings.

2017-05-31 Noemi Calace <noemi.calace@cern.ch>
	* Adding description for the InclinedQuads endcap supports and endcap pixel service routing

2017-05-17 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated stave support type identification for services placement
2017-05-16 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated stave support type, now defaults to Standard stave
2017-05-16 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Fixed longeron eos offset bug from previous commit
	* Renamed ShellThickness XML parameter to WallThickness
	* Added CornerThickness XML parameter
2017-05-16 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Added option for updated longeron shape using well-defined corners - can only be used by enabling hard-coded bool flag.
2017-05-13 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Fix IExtBrl4 XML
2017-05-09 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Fix Compiler warnings
2017-05-09 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Fix InclinedAlternative services collision
	* Tagged  as BarrelInclinedRef-00-00-63

2017-05-08 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Update ladder encvelopes to enclose services (inclined quads works, inclinedAlternative fail)
	* Fixed ladder envelopes for all layouts
2017-05-08 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Update services to go inside lognerons
	* BUG: Services outside ladder envelope
2017-05-03 Noemi Calace <noemi.calace@cern.ch>
	* Completing Alternative Inclined
	* Tagged as BarrelInclinedRef-00-00-62

2017-05-02 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Fixed Service placement
	* Tagged as BarrelInclinedRef-00-00-61

2017-04-27 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Added Phi rotational offset for barrel layers
	* Tagged as BarrelInclinedRef-00-00-59

2017-04-25 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated longeron parameters for different Z lengths
	* Updated longeron description for AlternativeInclined
	* Added XML parameter RadialMidpointEOS to control longeron position - layer envelopes adjusted to reach this position.
	* Added XML parameter MountainWidth for better control over mountain size

2017-04-25 Noemi Calace <noemi.calace@cern.ch>
	* Adding AlternativeInclined scripts
	* Tagged as BarrelInclinedRef-00-00-58

2017-04-25 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated XML reader to use new functions (removes confusing output messages)
	* Tagged as BarrelInclinedRef-00-00-57

2017-04-24 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated service positions
	* Enlarged Ladder Envelope
	* Tagged as BarrelInclinedRef-00-00-56

2017-04-24 Ben Smart <ben.smart@cern.ch>
	* Adding IPT

2017-04-24 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated service positions for new longeron layout (L2-L4)

2017-04-21 Noemi Calace <noemi.calace@cern.ch>
	* Adding disk support and routing for Inclined_Quads
	* Tagged as BarrelInclinedRef-00-00-52 and BarrelInclinedRef-00-00-53

2017-04-21 Noemi Calace <noemi.calace@cern.ch>
	* Adding disk support and routing for Inclined_Quads
	* Tagged as BarrelInclinedRef-00-00-50 and BarrelInclinedRef-00-00-51

2017-04-21 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated L2-L4 longeron material description
	* Added ability for one material per longeron type

2017-04-21 Ben Smart <ben.smart@cern.ch>
	* Fixing inclined quad support structure orientations (inner/outer) for L3 and L4

2017-04-20 Ben Smart <ben.smart@cern.ch>
	* Reducing inclined quad sensor support width by 5mm to avoid collisions with adjacent modules

2017-04-20 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Adjusted SlimStave XML, fixed collision between stave and modules

2017-04-20 Ben Smart <ben.smart@cern.ch>
	* Fixing inclined quad support masses, L1 orientation, IST radius, and endcap moderator thickness (7cm->5cm).

2017-04-20 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Fixed Ladder envelope radial offset, it now encloses the ladder components

2017-04-19 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated XML for Longerons, fixed collision.

2017-04-19 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated XML for InclinedQuads layout
	* Tagging as BarrelInclinedRef-00-00-47

2017-04-18 Ben Smart <ben.smart@cern.ch>
	* add code to automatically build inclined 'curly pipe' support if module is a quad

2017-04-07 Sharka Todorova <sarka.todorova@cern.ch>
	* add validation switch ( for barrel incl. layer )
	* Tagging as BarrelInclinedRef-00-00-46

2017-04-07 Sharka Todorova <sarka.todorova@cern.ch>
	* add full layer coverage to barrel layer validation tool
	* add 5 mm safety margin to layer envelope
	* Tagging as BarrelInclinedRef-00-00-45

2017-04-04 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Removed an include file that gave compilation error
	* Tagging as BarrelInclinedRef-00-00-44
2017-04-04 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Renamed Demonstrator stave to Slim
	* Fixed layer envelopes
	* Moved *Demonstrator* to *Slim*
2017-03-17 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated model of demonstrator stave (work in progress)
	* Demonstrator stave now configurable via xml, and can be turned on/off
	* Standard Stave support requires xml switch to be turned on
	* Tidied code
2017-03-09 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Updated model of demonstrator stave (work in progress)
2017-03-06 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Added first draft of demonstrator stave
	* Added switches to toggle construction of demonstrator stave and current stave
	* Removed dependence of GeoPixelLadderInclRef on StaveSupport object for module/service placement
	* Fix in svc offset, incl end-cap stave no longer intersects services with outer routing
	* Tagging as BarrelInclinedRef-00-00-42

2017-02-28 Sharka Todorova
	* import PixelLayerValidationTool
	* fix in svc offset
	* Tagging as BarrelInclinedRef-00-00-41

2017-02-27 Nathan Readioff <nathan.peter.readioff@cern.ch>
	* Removed dependence of GeoPixelEndcapModuleSvcRef on GeoPixelStaveSupportInclRef
	* Fixed error in Ladder envelope calculation
	* Fixed uninitialised pointer errors

2017-01-29 Noemi Calace <Noemi.Calace@cern.ch>
	* Adding routing for Optimised ring system for Inclined layout - alse IExtBrl
	* Tagged as BarrelInclinedRef-00-00-37

2017-01-29 Noemi Calace <Noemi.Calace@cern.ch>
	* Adding routing for Optimised ring system for Inclined layout
	* Tagged as BarrelInclinedRef-00-00-36

2016-11-30 Sharka Todorova
	* partial fix of bug in scaling of service module material
	* Tagging as BarrelInclinedRef-00-00-35

2016-10-27 Thorsten Kuhl <Thorsten.Kuhl@cern.ch>
	* cleaning up log file output (first iteration)
	* nil pointer protection for non existing material
	* Tagging as BarrelInclinedRef-00-00-34

2016-10-21 Ben Smart
	* Correcting stave masses for step 1.6 to keep total mass constant (=step 1.5)
	* Tagging as BarrelInclinedRef-00-00-33

2016-10-20 Simon Viel
       	* More last-minute adjustments to stave	masses for Step	1.6
       	* Tagging as BarrelInclinedRef-00-00-32

2016-10-19 Ben Smart
	* updating IExtBrl4 stave support structure materials, and Ti pipes for all layouts
	* Tagging as BarrelInclinedRef-00-00-31

2016-10-18 Ben Smart
	* updating IExtBrl4 stave support structure materials
	* Tagging as BarrelInclinedRef-00-00-30

2016-10-16 Sharka Todorova
	* fixed L3 barrel ladder in IExtBrl4
	* Tagging as BarrelInclinedRef-00-00-29

2016-10-15 Simon Viel
        * mv InclBrl_StaveSupport.xml IExtBrl4_StaveSupport.xml
        * didn't succeed in fixing IExtBrl4, something failed in 00-00-27
        * Tagging as BarrelInclinedRef-00-00-28

2016-10-14 Sharka Todorova
	* fixed module orientation
	* fixed mountain edge for IExtBrl4 (xml input added)
	* fixed missing svc transition
	* Tagging as BarrelInclinedRef-00-00-27

2016-10-14 Sharka Todorova
	* inner stave+svc routing for barrel L1,L3
	* Tagging as BarrelInclinedRef-00-00-26

2016-10-12 Ben Smart
	* updating pixel barrel cooling pipes
	* Tagging as BarrelInclinedRef-00-00-25

2016-10-06 Ben Smart
	* updating inclined stave materials and masses, and updating pixel chips
	* Tagging as BarrelInclinedRef-00-00-24

2016-09-30 Sharka Todorova
	* adding inclined module supports

2016-07-26 Ben Smart
	* moving moderator and reducing PP1 to bring pixel volume z-limit to 3475mm.
	* Tagging as BarrelInclinedRef-00-00-23

2016-06-03 Ben Smart
	* Merging BarrelInclinedRef-00-00-20-branch (BarrelInclinedRef-00-00-20-01) into the trunk
	* Tagging as BarrelInclinedRef-00-00-22.

2016-06-06 Sabine Elles
	* Add XML-CLOB interface
        * tagged as BarrelInclinedRef-00-00-20-01

2016-06-03 Ben Smart
	* Tagging as BarrelInclinedRef-00-00-21.

2016-05-25 Ben Smart
	* Updates to support structures and services.

2016-04-29 Sabine Elles
	* Fix CMakeLists.txt
	* tag as  BarrelInclinedRef-00-00-20	      

2016-04-28 Sabine Elles
	* Add CMakeLists.txt
	* tag as  BarrelInclinedRef-00-00-19	      
	  
2016-04-21 Ben Smart
	* Change thickness of pixel ring support tubes:	4.0mm to 0.4mm
	* tagged as BarrelInclinedRef-00-00-18

2016-04-11 Sabine Elles
	* Add Ecring carbon structure
	* Add layer0 services
	* Remove gap in services (and of stave of the outer layers)
	* tag as  BarrelInclinedRef-00-00-17
	
2016-04-04 Simon Viel
       	* Corrected LV cable weights (thanks Paul Miyagawa)
       	* tagged as BarrelInclinedRef-00-00-16

2016-03-17 Simon Viel
        * Updated data cable modularities following new input by Paolo Morettini
        * tagged as BarrelInclinedRef-00-00-15

2016-03-16 Simon Viel
       	* Twinax no longer defined as a material
       	* Innermost barrel cooling pipe	inner diameter fixed
        * tagged as BarrelInclinedRef-00-00-14

2016-03-15 Simon Viel
        * Update Twinax, Twisted pair and CO2 properties
        * Remove unnecessary <components> tab from xml
        * tagged as BarrelInclinedRef-00-00-13

2016-03-07 Simon Viel
        * Bug fix in GeoPixelEnvelopeExtInclTool.cxx to build IST and PST
       	* Smaller cooling pipes	for innermost barrel
        * Aluminium jacket for twisted-pair cables
        * Update again Twinax elemental composition by weight
        * tagged as BarrelInclinedRef-00-00-12

2016-03-04 Simon Viel
        * Fix Twinax and CO2 elemental composition by weight
        * tagged as BarrelInclinedRef-00-00-11

2016-03-03 Simon Viel
        * Harmonize service cable material between layouts
       	* Numbers derived from input by Paolo Morettini and Su Dong, 2016-02-19
        * tagged as BarrelInclinedRef-00-00-10

2016-02-16 Sabine Elles
        * Fix overlaps due to new barrel module tilt angles - OBO N. Calace 
        * tagged as BarrelInclinedRef-00-00-09

2016-02-11 Sabine Elles
        * Add full inclined layout geometry
        * tagged as BarrelInclinedRef-00-00-06 -07 and -08
	
2016-01-8 Noemi Calace <noemi.calace@cern.ch>
        * Removing PixelGeoModelUtils dependences
        * Removing compilation warnings
        * tagged as BarrelInclinedRef-00-00-03

2016-01-07 Noemi Calace <noemi.calace@cern.ch>
	* committing the trink
	* tagged as BarrelInclinedRef-00-00-02

2015-08-27 Sabine Elles
	* Create package
	* TAG as BarrelInclinedRef-00-00-01
	
