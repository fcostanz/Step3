# echo "cleanup PixelLayoutUtils PixelLayoutUtils-00-00-11 in /afs/cern.ch/user/c/czhou/temp_0314/Step3/InnerDetector/InDetDetDescr/PixelLayouts"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /afs/cern.ch/sw/contrib/CMT/v1r25p20160527
endif
source ${CMTROOT}/mgr/setup.csh
set cmtPixelLayoutUtilstempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtPixelLayoutUtilstempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=PixelLayoutUtils -version=PixelLayoutUtils-00-00-11 -path=/afs/cern.ch/user/c/czhou/temp_0314/Step3/InnerDetector/InDetDetDescr/PixelLayouts  -quiet -without_version_directory $* >${cmtPixelLayoutUtilstempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=PixelLayoutUtils -version=PixelLayoutUtils-00-00-11 -path=/afs/cern.ch/user/c/czhou/temp_0314/Step3/InnerDetector/InDetDetDescr/PixelLayouts  -quiet -without_version_directory $* >${cmtPixelLayoutUtilstempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtPixelLayoutUtilstempfile}
  unset cmtPixelLayoutUtilstempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtPixelLayoutUtilstempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtPixelLayoutUtilstempfile}
unset cmtPixelLayoutUtilstempfile
exit $cmtcleanupstatus

