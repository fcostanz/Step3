# echo "setup PixelLayoutUtils PixelLayoutUtils-00-00-11 in /afs/cern.ch/user/c/czhou/temp_0314/Step3/InnerDetector/InDetDetDescr/PixelLayouts"

if test "${CMTROOT}" = ""; then
  CMTROOT=/afs/cern.ch/sw/contrib/CMT/v1r25p20160527; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtPixelLayoutUtilstempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtPixelLayoutUtilstempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=PixelLayoutUtils -version=PixelLayoutUtils-00-00-11 -path=/afs/cern.ch/user/c/czhou/temp_0314/Step3/InnerDetector/InDetDetDescr/PixelLayouts  -quiet -without_version_directory -no_cleanup $* >${cmtPixelLayoutUtilstempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=PixelLayoutUtils -version=PixelLayoutUtils-00-00-11 -path=/afs/cern.ch/user/c/czhou/temp_0314/Step3/InnerDetector/InDetDetDescr/PixelLayouts  -quiet -without_version_directory -no_cleanup $* >${cmtPixelLayoutUtilstempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtPixelLayoutUtilstempfile}
  unset cmtPixelLayoutUtilstempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtPixelLayoutUtilstempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtPixelLayoutUtilstempfile}
unset cmtPixelLayoutUtilstempfile
return $cmtsetupstatus

