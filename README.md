Repository for ITk Step3.0



Setup
-------
clone the project then go into the project and do:

```
asetup 20.20.10.2,here
```

First time compilation can be done with

```
cmt find_packages
cmt compile
```


Making a run directory
----------

You should do your running inside a run directory. Just create any dir under the ```run``` dir. E.g.

```
mkdir $TestArea/run/local
```

run from in there


Running VP1
-----------
Can be done from your run directory with:

```
athena $TestArea/run/will/VP1JobOptions.py
```

Running Extrapolation
--------------
Can be done with:

```
athena $TestArea/run/will/extrapolationJobOptions.py
```

Changing the Layout
----------------
Both scripts you can change to a different layout using the extra option:

```
-c "LayoutOption='InclinedQuads'
```

The default LayoutOption is ```InclinedAlternative```
